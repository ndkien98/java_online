USE csdlhangkhong;

-- 1

SELECT *
FROM chuyenbay
WHERE GaDen = 'DAD';

-- 2

SELECT *
FROM maybay
WHERE TamBay > 10000;

-- 3

SELECT *
FROM nhanvien
WHERE Luong < 10000;

-- 4

SELECT *
FROM chuyenbay
where DoDai > 8000
  AND DoDai < 10000;

SELECT *
FROM chuyenbay
WHERE DoDai BETWEEN 8000 AND 10000;

-- 5

SELECT *
FROM chuyenbay
WHERE GaDi = 'SGN'
  AND GaDen = 'BMV';

-- 6

SELECT count(1) as numberChuyenBay
FROM chuyenbay
WHERE GaDi = 'SGN';

-- 7
SELECT count(1) as so_loai_boeing
FROM maybay
WHERE Loai LIKE '%Boeing%';

-- 8

SELECT SUM(Luong)
FROM nhanvien;

-- 8

SELECT DISTINCT nhanvien.Ten
FROM nhanvien
         JOIN chungnhan c on nhanvien.MaNV = c.MaNV
         JOIN maybay m on c.MaMB = m.MaMB AND m.Loai LIKE '%Boeing%';


-- 10
SELECT chungnhan.MaNV
FROM chungnhan
         JOIN maybay m on chungnhan.MaMB = m.MaMB
WHERE m.MaMB = '747';

-- 11

SELECT DISTINCT c.MaMB
FROM nhanvien
         JOIN chungnhan c on nhanvien.MaNV = c.MaNV
WHERE nhanvien.Ten LIKE 'Nguyen%';

-- 12

SELECT *
FROM (
         SELECT DISTINCT chungnhan.MaNV
         FROM chungnhan
                  JOIN maybay m on chungnhan.MaMB = m.MaMB
         WHERE m.Loai LIKE 'Airbus%'
     ) Airbus
         JOIN (
    SELECT DISTINCT chungnhan.MaNV
    FROM chungnhan
             JOIN maybay m on chungnhan.MaMB = m.MaMB
    WHERE m.Loai LIKE 'Boeing%'
) Boeing ON Airbus.MaNV = Boeing.MaNV;


-- 13

SELECT *
FROM maybay
WHERE TamBay > 11979;


-- 14

SELECT *
FROM chuyenbay
WHERE DoDai < 4168;

-- 15

SELECT DISTINCT nhanvien.Ten
FROM nhanvien
         JOIN chungnhan c on nhanvien.MaNV = c.MaNV
         JOIN maybay m on c.MaMB = m.MaMB AND m.Loai LIKE '%Boeing%';

-- 16 Với mỗi loại máy bay có phi công lái cho biết mã số, loại máy báy và tổng số phi công có thể lái
--   loại máy bay ñó.

-- những máy bay có phi công lái
SELECT maybay.MaMB, maybay.Loai, COUNT(MaNV)
FROM maybay
         JOIN chungnhan c on maybay.MaMB = c.MaMB
GROUP BY (maybay.MaMB)
;

-- 17  Giả sử một hành khách muốn ñi thẳng từ ga A ñến ga B rồi quay trở về ga A. Cho biết các ñường
-- bay nào có thể ñáp ứng yêu cầu này.


-- 18 Với mỗi ga có chuyến bay xuất phát từ ñó cho biết có bao nhiêu chuyến bay khởi hành từ ga ñó.

SELECT GaDi, COUNT(MaCB) AS numberFlight
FROM chuyenbay
group by (GaDi);

-- 19 Với mỗi ga có chuyến bay xuất phát từ ñó cho biết tổng chi phí phải trả cho phi công lái các
-- chuyến bay khởi hành từ ga ñó

SELECT GaDi, COUNT(MaCB) AS numberFlight, SUM(ChiPhi) AS totalPrice
FROM chuyenbay
group by (GaDi);

-- 20 Với mỗi ñịa ñiểm xuất phát cho biết có bao nhiêu chuyến bay có thể khởi hành trước 12:00.

SELECT GaDi, COUNT(MaCB) AS numberFlight, GioDi
FROM chuyenbay
WHERE GioDi < '12:00:00'
group by (GaDi);


-- 21 Cho biết mã số của các phi công chỉ lái ñược 3 loại máy bay
SELECT *
FROM (SELECT MaNV, COUNT(MaMB) AS numberAirp FROM chungnhan GROUP BY (MaMB)) AS a
WHERE a.numberAirp = 3;

-- 22 Với mỗi phi công có thể lái nhiều hơn 3 loại máy bay, cho biết mã số phi công và tầm bay lớn
-- nhất của các loại máy bay mà phi công ñó có thể lái.

SELECT a.MaNV, MAX(maybay.TamBay)
FROM (SELECT *, COUNT(MaMB) AS numberAirp FROM chungnhan GROUP BY (MaMB)) AS a
         JOIN maybay on a.MaMB = maybay.MaMB
    AND a.numberAirp > 3
GROUP BY (maybay.MaMB);

-- 23
SELECT DISTINCT nhanvien.MaNV, nhanvien.Ten, COUNT(c.MaMB) AS TotalCategoryAirport
FROM nhanvien
         JOIN chungnhan c on nhanvien.MaNV = c.MaNV
GROUP BY c.MaNV;

-- 24  Cho biết mã số của các phi công có thể lái ñược nhiều loại máy bay nhất.
SELECT *
FROM (SELECT DISTINCT nhanvien.MaNV, nhanvien.Ten, COUNT(c.MaMB) AS TotalCategoryAirport
      FROM nhanvien
               JOIN chungnhan c on nhanvien.MaNV = c.MaNV
      GROUP BY c.MaNV) AS NV
WHERE NV.TotalCategoryAirport = (SELECT MAX(NV2.TotalCategoryAirport)
                                 FROM (SELECT DISTINCT nhanvien.MaNV,
                                                       nhanvien.Ten,
                                                       COUNT(c.MaMB) AS TotalCategoryAirport
                                       FROM nhanvien
                                                JOIN chungnhan c on nhanvien.MaNV = c.MaNV
                                       GROUP BY c.MaNV) AS NV2);


-- 25 Cho biết mã số của các phi công có thể lái ñược ít loại máy bay nhất.
SELECT *
FROM (SELECT DISTINCT nhanvien.MaNV, nhanvien.Ten, COUNT(c.MaMB) AS TotalCategoryAirport
      FROM nhanvien
               JOIN chungnhan c on nhanvien.MaNV = c.MaNV
      GROUP BY c.MaNV) AS NV
WHERE NV.TotalCategoryAirport = (SELECT MIN(NV2.TotalCategoryAirport)
                                 FROM (SELECT DISTINCT nhanvien.MaNV,
                                                       nhanvien.Ten,
                                                       COUNT(c.MaMB) AS TotalCategoryAirport
                                       FROM nhanvien
                                                JOIN chungnhan c on nhanvien.MaNV = c.MaNV
                                       GROUP BY c.MaNV) AS NV2);

-- 26 Tìm các nhân viên không phải là phi công
SELECT *
FROM nhanvien
WHERE MaNV NOT IN (SELECT DISTINCT nhanvien.MaNV
                   FROM nhanvien
                            JOIN chungnhan c on nhanvien.MaNV = c.MaNV);

-- 27 Cho biết mã số của các nhân viên có lương cao nhất.

SELECT nhanvien.MaNV
FROM nhanvien
WHERE Luong = (SELECT MAX(nhanvien.Luong) FROM nhanvien);

-- 28 Cho biết tổng số lương phải trả cho các phi công

SELECT COUNT(nhanvien.Luong)
FROM nhanvien
WHERE MaNV IN (SELECT DISTINCT nhanvien.MaNV
               FROM nhanvien
                        JOIN chungnhan c on nhanvien.MaNV = c.MaNV);

-- 29  Tìm các chuyến bay có thể ñược thực hiện bởi tất cả các loại máy bay Boeing

-- tim ra min tam bay cua boeing
-- tim tat ca chuyen bay > min tam bay

SELECT *
FROM chuyenbay
WHERE DoDai <= (SELECT MIN(TamBay) FROM maybay WHERE Loai LIKE "boeing%");


-- 30 Cho biết mã số của các máy bay có thể ñược sử dụng ñể thực hiện chuyến bay từ Sài Gòn (SGN)
-- ñến Huế (HUI).

SELECT MaMB
FROM maybay
WHERE TamBay >= (SELECT TamBay FROM chuyenbay WHERE GaDi = 'SGN' AND GaDen = 'HUI');

-- 31 Tìm các chuyến bay có thể ñược lái bởi các phi công có lương lớn hơn 100,000

-- tim ra phi cong co luong > 100,000

-- Tim ra may bay duoc thuc hien boi phi cong day
SELECT TamBay
FROM maybay
WHERE MaMB IN (SELECT DISTINCT MaMB
               FROM nhanvien
                        JOIN chungnhan c on nhanvien.MaNV = c.MaNV
               WHERE Luong > 100000);


-- Tim ra chuyen bay duoc thuc hien boi may bay day

-- 32  Cho biết tên các phi công có lương nhỏ hơn chi phí thấp nhất của ñường bay từ Sài Gòn (SGN)

-- ñến Buôn Mê Thuộc (BMV)

-- chi phi thap nhat cua duong bay tuw SGN -> BMV

-- Phi cong co luong nho hon luong day

SELECT DISTINCT nhanvien.Ten
FROM nhanvien
         JOIN chungnhan c on nhanvien.MaNV = c.MaNV
WHERE Luong < (SELECT MIN(ChiPhi) FROM chuyenbay WHERE GaDi = 'SGN' AND GaDen = 'BMV');

-- 33 Cho biết mã số của các phi công có lương cao nhất

-- lay ra luong cao nhat cua cac phi cong

-- loc cac phi cong theo luong day

SELECT DISTINCT nhanvien.MaNV
FROM nhanvien
         JOIN chungnhan c on nhanvien.MaNV = c.MaNV
WHERE Luong = (SELECT MAX(Luong)
               FROM nhanvien
                        JOIN chungnhan c on nhanvien.MaNV = c.MaNV);

-- 34 Cho biết mã số của các nhân viên có lương cao thứ nhì

-- tim ra luong cao nhat

-- tim ra nhan vien co luong max < luong cao nhat

SELECT *
FROM nhanvien
WHERE Luong = (SELECT MAX(luong) FROM nhanvien WHERE Luong < (SELECT MAX(luong) FROM nhanvien));

-- 35  Cho biết mã số của các nhân viên có lương cao thứ nhất hoặc thứ nhì.

SELECT *
FROM nhanvien
WHERE Luong = (SELECT MAX(luong) FROM nhanvien)
   OR Luong = (SELECT MAX(luong) FROM nhanvien WHERE Luong < (SELECT MAX(luong) FROM nhanvien));


-- 36 Cho biết tên và lương của các nhân viên không phải là phi công và có lương lớn hơn lương trung
-- bình của tất cả các phi công.

-- luong trung binh cua tat ca phi cong

-- so cac phi cong
SELECT COUNT(PC.MaNV)
FROM (SELECT DISTINCT nhanvien.MaNV
      FROM nhanvien
               JOIN chungnhan c on nhanvien.MaNV = c.MaNV) AS PC;
-- tong luong cua tat ca cac phi cong

SELECT SUM(PC.Luong) / (SELECT COUNT(PC.MaNV)
                        FROM (SELECT DISTINCT nhanvien.MaNV
                              FROM nhanvien
                                       JOIN chungnhan c on nhanvien.MaNV = c.MaNV) AS PC
)
FROM (SELECT DISTINCT nhanvien.MaNV, Luong
      FROM nhanvien
               JOIN chungnhan c on nhanvien.MaNV = c.MaNV) AS PC;

-- 37 Cho biết tên các phi công có thể lái các máy bay có tầm bay lớn hơn 4,800km nhưng không có
-- chứng nhận lái máy bay Boeing.

-- cac phi cong lai duoc boeing

SELECT DISTINCT MaNV
FROM chungnhan
         JOIN maybay m on chungnhan.MaMB = m.MaMB
WHERE Loai LIKE "boeing%";
-- cac phi cong khon lai duoc boeing

SELECT DISTINCT MaNV, MaMB
FROM chungnhan
WHERE MaNV NOT IN (SELECT DISTINCT MaNV
                   FROM chungnhan
                            JOIN maybay m on chungnhan.MaMB = m.MaMB
                   WHERE Loai LIKE "boeing%");

-- MaNV nao co lai duoc tam bay > 4,800km

SELECT nv.Ten
FROM (SELECT DISTINCT MaNV, MaMB
      FROM chungnhan
      WHERE MaNV NOT IN (SELECT DISTINCT MaNV
                         FROM chungnhan
                                  JOIN maybay m on chungnhan.MaMB = m.MaMB
                         WHERE Loai LIKE "boeing%")) AS ChungNhan2
         JOIN maybay m ON ChungNhan2.MaMB = m.MaMB
         JOIN nhanvien nv ON ChungNhan2.MaNV = nv.MaNV
WHERE m.TamBay > 4800;

-- 38  Cho biết tên các phi công lái ít nhất 3 loại máy bay có tầm bay xa hơn 3200km.

-- may loai bay co tam bay > 3200

--  so loai may bay co tam bay > 3200 ma phi cong co the lai

-- so sanh voi 3

-- lay ra ten nv

SELECT Ten
FROM nhanvien
WHERE nhanvien.MaNV IN (SELECT CN2.MaNV
                        FROM (SELECT MaNV, COUNT(Loai) AS SoLoai
                              FROM chungnhan
                                       JOIN (SELECT * FROM maybay WHERE TamBay > 3200) AS MB2
                                            ON chungnhan.MaMB = MB2.MaMB
                              GROUP BY (MaNV)) AS CN2
                        WHERE CN2.SoLoai >= 3);

-- 39 Với mỗi nhân viên cho biết mã số, tên nhân viên và tổng số loại máy bay mà nhân viên ñó có thể
-- lái.

-- nhan vien la phi cong

SELECT Ten, nhanvien.MaNV, COUNT(m.Loai)
FROM nhanvien
         JOIN chungnhan c on nhanvien.MaNV = c.MaNV
         JOIN maybay m on c.MaMB = m.MaMB
GROUP BY (nhanvien.MaNV);

-- Join voi bang may bay nhom theo manv de lay ra so loai

-- 40 Với mỗi nhân viên cho biết mã số, tên nhân viên và tổng số loại máy bay Boeing mà nhân viên
-- ñó có thể lái.
SELECT Ten, nhanvien.MaNV, COUNT(m.Loai)
FROM nhanvien
         JOIN chungnhan c on nhanvien.MaNV = c.MaNV
         JOIN maybay m on c.MaMB = m.MaMB
WHERE m.Loai LIKE "Boeing%"
GROUP BY (nhanvien.MaNV);

-- 41 Với mỗi loại máy bay cho biết loại máy bay và tổng số phi công có thể lái loại máy bay ñó

SELECT Loai, COUNT(MaNV)
FROM maybay
         JOIN chungnhan c on maybay.MaMB = c.MaMB
GROUP BY (Loai);


-- 42 Với mỗi loại máy bay cho biết loại máy bay và tổng số chuyến bay không thể thực hiện bởi loại
-- máy bay ñó

-- do dai > tam bay => khong the thuc hien


-- 43 Với mỗi loại máy bay cho biết loại máy bay và tổng số phi công có lương lớn hơn 100,000 có thể lái loại máy bay ñó.

-- Phi cong co luong > 100000
use csdlhangkhong;

SELECT Loai, MB2.TongPC
FROM maybay
         JOIN (SELECT DISTINCT C.MaMB, COUNT(c.MaNV) AS TongPC
               FROM nhanvien
                        JOIN chungnhan c on nhanvien.MaNV = c.MaNV
               WHERE Luong > 100000
               GROUP BY (MaMB)) AS MB2 ON maybay.MaMB = MB2.MaMB;


-- 44 Với mỗi loại máy bay có tầm bay trên 3200km, cho biết tên của loại máy bay và lương trung bình của các phi công có thể lái loại máy bay ñó

-- may bay co tam bay ? 3200
SELECT MaMB, Loai
FROM maybay
WHERE TamBay > 3200;

-- Tim ra phi cong lai loai may bay do

SELECT chungnhan.MaNV, mayBay2.MaMB, mayBay2.Loai
FROM chungnhan
         JOIN (SELECT MaMB, Loai
               FROM maybay
               WHERE TamBay > 3200) AS mayBay2 ON chungnhan.MaMB = mayBay2.MaMB ORDER BY chungnhan.MaMB ASC ;

-- tinh tong luong phi cong theo tung loai

    #   input : table MB3 , maybay.MMB
    # tao ra bien chua luong cua cac phi cong
    # duyet table neu thay

    # output : table : MMB, LTB cac phi cong




-- 45 Với mỗi loại máy bay cho biết loại máy bay và tổng số nhân viên không thể lái loại máy bay ñó.

# nhan vien khon duoc cap chung chi se khong duoc lai may bay do3

    # tim ra tat ca msv la phi cong

